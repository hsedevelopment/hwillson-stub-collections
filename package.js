/* global Package, Npm */
/* eslint-disable prefer-arrow-callback */

Package.describe({
  name: 'hwillson:stub-collections',
  version: '1.0.9-dev',
  summary: 'Stub out Meteor collections with in-memory local collections.',
  documentation: 'README.md',
  git: 'https://github.com/hwillson/meteor-stub-collections.git',
  debugOnly: true,
});

Npm.depends({
  sinon: '7.3.2',
  chai: '4.2.0',
});

Package.onUse(function onUse(api) {
  api.versionsFrom('2.0');
  api.use([
    'ecmascript',
    'mongo',
  ]);
  api.mainModule('index.js');
});

Package.onTest(function onTest(api) {
  api.use([
    'ecmascript',
    'mongo',
    'aldeed:collection2@3.2.1',
    'meteortesting:mocha@2.0.3',
    'hwillson:stub-collections',
  ]);
  api.mainModule('tests.js');
});
